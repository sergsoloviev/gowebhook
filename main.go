package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/sergsoloviev/gowebhook/conf"
	"bitbucket.org/sergsoloviev/gowebhook/views"
)

var (
	Conf *conf.Configuration
)

func main() {
	Conf, err := conf.Read()
	if err != nil {
		panic(err)
	}
	logfile, err := os.OpenFile(Conf.Log, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	log.SetOutput(logfile)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		views.PostPush(w, r, Conf)
	})

	listen := fmt.Sprintf("%s:%s", Conf.Listen.Ip, Conf.Listen.Port)
	log.Println("http://" + listen)
	http.ListenAndServe(listen, nil)

}
