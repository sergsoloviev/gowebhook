package conf

import (
	"encoding/json"
	"io/ioutil"
)

type Configuration struct {
	Listen struct {
		Ip   string `json: "ip"`
		Port string `json: "port"`
	}
	Log     string `json: "log"`
	RepoDir string `json: "repodir"`
	Uid     uint32 `json: "uid"`
	Gid     uint32 `json: "gid"`
}

func Read(params ...string) (conf *Configuration, err error) {
	file_name := "/etc/gowebhook/config.json"
	if len(params) > 0 {
		file_name = params[0]
	}
	bytes, err := ioutil.ReadFile(file_name)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(bytes, &conf)
	if err != nil {
		return nil, err
	}
	return conf, nil
}
