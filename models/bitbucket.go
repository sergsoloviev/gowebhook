package models

type PostPush struct {
	Push       Push       `json:"push"`
	Repository Repository `json:"repository"`
	Actor      Actor      `json:"actor"`
}

type Repository struct {
	Name     string  `json:"name"`
	Project  Project `json:"project"`
	FullName string  `json:"full_name"`
	Owner    Owner   `json:"owner"`
}

type Project struct {
	Name string `json:"name"`
	Key  string `json:"key"`
}

type Actor struct {
	Username    string `json:"username"`
	DisplayName string `json:"display_name"`
}

type Push struct {
	Changes []Change `json:"changes"`
}

type Change struct {
	Old News `json:"old"`
	New News `json:"new"`
}

type News struct {
	Type string `json:"type"`
	Name string `json:"name"`
}

type Owner struct {
	Username string `json:"username"`
}
