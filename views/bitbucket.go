package views

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os/exec"
	"strings"
	"syscall"

	"bitbucket.org/sergsoloviev/gowebhook/conf"
	"bitbucket.org/sergsoloviev/gowebhook/models"
)

func PostPush(w http.ResponseWriter, r *http.Request, conf *conf.Configuration) {
	context := responseJSON{}

	decoder := json.NewDecoder(r.Body)
	var post models.PostPush
	err := decoder.Decode(&post)
	if err != nil {
		log.Println(err)
		response400(w, context)
		return
	}
	defer r.Body.Close()

	is_branch_ok := false
	var branch, master string

	for _, change := range post.Push.Changes {
		branch = change.New.Type
		master = change.New.Name
		if branch == "branch" && master == "master" {
			is_branch_ok = true
		}
	}

	if is_branch_ok {
		go git_pull(conf)
	}
	response(w, context)
}

func git_pull(conf *conf.Configuration) {
	shell_command(conf, "git", "pull", "origin", "master")
}

func shell_command(conf *conf.Configuration, params ...string) {
	log.Printf("cmd: %s", strings.Join(params[:], " "))
	name := params[0]
	arg := params[1:]

	var outb, errb bytes.Buffer

	cmd := exec.Command(name)
	if len(arg) > 0 {
		cmd = exec.Command(name, arg...)
	}
	//if hostuser != "root" {
	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.Credential = &syscall.Credential{Uid: conf.Uid, Gid: conf.Gid}
	//}
	cmd.Dir = conf.RepoDir
	cmd.Stdout = &outb
	cmd.Stderr = &errb
	err := cmd.Run()
	if err != nil {
		log.Println(err)
	}
	log.Print("out: ", outb.String())
	log.Println("err: ", errb.String())
}
