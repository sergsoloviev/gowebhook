package views

import (
	"encoding/json"
	"net/http"
)

type errorJson struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type responseJSON struct {
	Error *errorJson  `json:"error"`
	Data  interface{} `json:"data"`
	Prev  string      `json:"prev"`
	Next  string      `json:"next"`
}

func response(w http.ResponseWriter, context responseJSON) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(context)
}

func response201(w http.ResponseWriter, context responseJSON) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(context)
}

func response400(w http.ResponseWriter, context responseJSON) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(context)
}

func response404(w http.ResponseWriter, context responseJSON) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusNotFound)
	json.NewEncoder(w).Encode(context)
}
